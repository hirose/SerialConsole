This project is a tool to control TEXIO and Keithley 24XX series power supplies.
And, this can be used as a library for serial communication with RS232 cable.


## manual
control TEXIO PS :
http://osksn2.hep.sci.osaka-u.ac.jp/~hirose/research/texio_control.html

control Keithley PS :
http://osksn2.hep.sci.osaka-u.ac.jp/~hirose/research/keithley_control.html