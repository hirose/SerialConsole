// texio_if_41rs.h
#ifndef INCLUDED_TEXIO_IF_41RS
#define INCLUDED_TEXIO_IF_41RS

#include"serial_interface.h"
#include<termios.h>

//#define DEBUG

//Interface type
enum itype{usb, rs232};

class texio_if_41rs : public serial_interface
{
 public:
  texio_if_41rs();
  ~texio_if_41rs();

  int initialize();
  int finalize();

  int set_address(int address){m_address = address;};

  int write(std::string command);
  std::string read(std::string &buffer);

  int make_listener();
  int make_talker();

 private:

  int fd;
  struct termios oldtio, newtio;
  char* buf;

 protected:

};

#endif
