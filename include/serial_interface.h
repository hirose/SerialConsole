// serial_interface.h
#ifndef INCLUDED_SERIAL_INTERFACE
#define INCLUDED_SERIAL_INTERFACE
#include<string>

class serial_interface
{
 public:
  serial_interface(){};
  virtual ~serial_interface(){};

  virtual int initialize(){};
  virtual int finalize(){};

  bool is_initialized(){return m_initialized;};

  int get_address(){return m_address;};
  std::string get_device_name(){return m_device_name;};

  virtual int set_address(int address){}; //This should be implemented in a daughter class.
  void set_device_name(std::string device_name){m_device_name = device_name;};

  virtual int create_tcp_socket(){};
  virtual int set_ip_address(std::string address){};
  virtual int set_tcp_port(unsigned int tcp_port){};
  virtual int set_udp_port(unsigned int udp_port){};

  virtual int get_tcp_socket(){};
  virtual int close_tcp_socket(){};

  virtual int write(std::string command){};
  virtual std::string read(std::string &buffer){};

  virtual int make_listener(){};
  virtual int make_talker(){};

 protected:
  int m_address;
  std::string m_device_name;
  int m_initialized;

};

#endif
