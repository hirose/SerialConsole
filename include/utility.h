// utility.h
#ifndef INCLUDED_UTILITY
#define INCLUDED_UTILITY
#include<fcntl.h>
#include<termios.h>

std::string get_time_str()
{
  std::string str_time;

  // //C++11 (how to get timezone...?)
  // auto read_time = std::chrono::system_clock::now();
  // std::time_t t = std::chrono::system_clock::to_time_t(read_time);
  // std::cout << std::ctime(&t) << std::endl;

  //Getting time by an old C-style.
  time_t now_t = time(NULL);
  struct tm *read_time = localtime(&now_t);
  char char_time[256];
  size_t time_result = strftime(char_time, sizeof(char_time), "%c %z", read_time);
  str_time = char_time;

  return str_time;
}

std::string get_out_format(double voltage, double current)
{
  std::string str_out;
  char out_format[256];
  sprintf(out_format, "%s : Voltage = %g [V], Current = %g [A]", get_time_str().c_str(), voltage, current);
  str_out = out_format;

  return str_out;
}

void clearStdin()
{
  //make sure to discard stdin buffer contents before using next scanf() and fgets()
  int c;
  while ((c = getchar()) != '\n' && c != EOF);
}

//checking keyboard hits
int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch!=EOF){
    ungetc(ch, stdin);
    return 1;
  }
  return 0;
}

#endif
