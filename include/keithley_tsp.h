// keithley_tsp.h
#ifndef INCLUDED_KEITHLEY_TSP
#define INCLUDED_KEITHLEY_TSP

#include"serial_interface.h"
#include<arpa/inet.h>
#include<sys/ioctl.h>
#include<fcntl.h>

//#define DEBUG

class keithley_tsp : public serial_interface
{
 public:
  keithley_tsp();
  ~keithley_tsp();

  int initialize();
  int finalize();

  int set_address(int address);

  int write(std::string command);
  std::string read(std::string &buffer);

  int make_listener();
  int make_talker();

  int set_ip_address(std::string address){sitcpIpAddr = address.c_str();};
  int set_tcp_port(unsigned int tcp_port){tcpPort = tcp_port;};
  int set_udp_port(unsigned int udp_port){udpPort = udp_port;};
  int create_tcp_socket();
  int get_tcp_socket(){return tcpsock;}
  int close_tcp_socket();

 private:
  std::string sitcpIpAddr;
  unsigned int tcpPort;
  unsigned int udpPort;
  int udpsock;
  int tcpsock;

  fd_set rmask, wmask, readfds, fds;
  struct timeval timeout;

 protected:
  struct sockaddr_in tcpAddr;
  struct sockaddr_in udpAddr;

};

#endif
