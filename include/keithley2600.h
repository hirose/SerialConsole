// keithley2600.h
#ifndef INCLUDED_KEITHLEY2600
#define INCLUDED_KEITHLEY2600

#include"power_supply.h"

class keithley2600 : public power_supply
{
 public:
  keithley2600(){};
  keithley2600(int address){m_address = address;};
  ~keithley2600(){};

  int initialize(serial_interface *si = nullptr);
  int finalize(serial_interface *si = nullptr);

  int reset(serial_interface *si);
  int configure(serial_interface *si);
  int power_on(serial_interface *si);
  int power_off(serial_interface *si);
  int config_voltage(serial_interface *si);
  int config_compliance(serial_interface *si);
  void set_operating_channel(int operating_channel);
  double read_voltage(serial_interface *si);
  double read_current(serial_interface *si);
  void read_voltage_and_current(serial_interface *si, double &voltage, double &current);

  int voltage_sweep(serial_interface *si);

  bool is_on(serial_interface *si);
  bool is_off(serial_interface *si);

  void set_sweep_steps(int sweep_steps){m_sweep_steps = sweep_steps;};
  void set_sweep_sleep_in_ms(int sweep_sleep_in_ms){m_sweep_sleep_in_ms = sweep_sleep_in_ms;};

  std::string get_device_type(){return "KEITHLEY2600";};

 private:
  std::string m_str_channel = "";

};

#endif
