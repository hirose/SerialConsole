// keithley2410.h
#ifndef INCLUDED_KEITHLEY2410
#define INCLUDED_KEITHLEY2410

#include"power_supply.h"

class keithley2410 : public power_supply
{
 public:
  keithley2410(){};
  keithley2410(int address){m_address = address;};
  ~keithley2410(){};

  int initialize(serial_interface *si = nullptr);
  int finalize(serial_interface *si = nullptr);

  int reset(serial_interface *si);
  int configure(serial_interface *si);
  int power_on(serial_interface *si);
  int power_off(serial_interface *si);
  int config_voltage(serial_interface *si);
  int config_compliance(serial_interface *si);
  double read_voltage(serial_interface *si);
  double read_current(serial_interface *si);
  void read_voltage_and_current(serial_interface *si, double &voltage, double &current);

  int voltage_sweep(serial_interface *si);

  bool is_on(serial_interface *si);
  bool is_off(serial_interface *si);

  void set_sweep_steps(int sweep_steps){m_sweep_steps = sweep_steps;};
  void set_sweep_sleep_in_ms(int sweep_sleep_in_ms){m_sweep_sleep_in_ms = sweep_sleep_in_ms;};

  std::string get_device_type(){return "KEITHLEY2410";};

 private:
  double m_compliance[256] = {105E-6};

};

#endif
