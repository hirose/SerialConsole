// keithley_24XX_rs232.h
#ifndef INCLUDED_KEITHLEY_24XX_RS232
#define INCLUDED_KEITHLEY_24XX_RS232

#include"serial_interface.h"
#include<termios.h>

//#define DEBUG

//Interface type
enum itype{usb, rs232};

class keithley_24XX_rs232 : public serial_interface
{
 public:
  keithley_24XX_rs232();
  ~keithley_24XX_rs232();

  int initialize();
  int finalize();

  int set_address(int address){m_address = address;};

  int write(std::string command);
  std::string read(std::string &buffer);

  int make_listener();
  int make_talker();

 private:

  int fd;
  struct termios oldtio, newtio;
  char* buf;

 protected:

};

#endif
