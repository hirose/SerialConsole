CXX    = g++

SRCDIR = src
INCDIR = include
OBJDIR = lib
BINDIR = bin
UTILDIR = util

CXXFLAGS = -I./$(INCDIR) -std=c++11

vpath %.cxx $(SRCDIR)
vpath %.cxx $(UTILDIR)
vpath %.h $(INCDIR)
vpath %.o $(OBJDIR)
vpath %.d $(OBJDIR)

SOURCES = $(wildcard $(SRCDIR)/*.cxx)
LIB_OBJS := $(patsubst %.cxx,$(OBJDIR)/%.o,$(notdir $(SOURCES)))
ALL_OBJS := $(patsubst %.cxx,$(OBJDIR)/%.o,$(notdir $(SOURCES)))
ALL_DEPS := $(patsubst %.cxx,$(OBJDIR)/%.d,$(notdir $(SOURCES)))

UTILS = $(wildcard $(UTILDIR)/*.cxx)
ALL_OBJS += $(patsubst %.cxx,$(OBJDIR)/%.o,$(notdir $(UTILS)))
ALL_DEPS += $(patsubst %.cxx,$(OBJDIR)/%.d,$(notdir $(UTILS)))

TARGETS := $(patsubst %.cxx,$(BINDIR)/%,$(notdir $(UTILS)))

.PHONY: all clean test

all: ${TARGETS}

${TARGETS}: ${ALL_OBJS}
	${CXX} ${CXXFLAGS} -o $@ ${LIB_OBJS} $(patsubst bin/%,lib/%.o,$@)

$(OBJDIR)/%.o: %.cxx
	${CXX} -MMD -MP ${CXXFLAGS} -c $< -o $@

clean:
	@echo "-- Now Clean Up --"
	rm -f *~ $(TARGETS) $(OBJDIR)/*.d $(OBJDIR)/*.o $(BINDIR)/* $(SRCDIR)/#* $(UTILDIR)/#*

test:
	@echo $(TARGETS)
	@echo $(SOURCES)
	@echo $(ALL_OBJS)
	@echo $(LIB_OBJS)
	@echo $(DEPS)

-include $(ALL_DEPS)
