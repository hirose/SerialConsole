#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>

#include"serial_interface.h"
#include"keithley2410.h"
#include"SerialCom.h"

int keithley2410::initialize(serial_interface *si)
{
  return 0;
}

int keithley2410::finalize(serial_interface *si)
{
  return 0;
}

int keithley2410::reset(serial_interface *si)
{
  si->set_address(m_address);
  si->write("*RST");
  return 0;
}

int keithley2410::configure(serial_interface *si)
{
  si->set_address(m_address);
  si->make_listener();

  //Making Voltage source mode with fixed Voltage
  si->write(":SOURCE:FUNCTION VOLTAGE");
  si->write(":SOURCE:VOLTAGE:MODE FIXED");
  si->write(":SOURCE:VOLTAGE:RANGE MAX");

  //Measurement settings
  si->write(":TRIGGER:COUNT 1");
  si->write(":SENSE:CURRENT:RANGE 10E-6");
  si->write(":SENSE:FUNCTION 'CURR'");
  si->write(":SENSE:FUNCTION 'VOLT'");
  si->write(":FORMAT:ELEMENTS VOLTAGE,CURRENT");

  //Configuring Voltage and the current compliance.
  si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance[m_operating_channel]));
  si->write(":SOURCE:VOLTAGE:LEVEL "+std::to_string(m_voltage[m_operating_channel]));

  return 0;
}

int keithley2410::power_on(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":OUTPUT ON");

  return 1;
}

int keithley2410::power_off(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":OUTPUT OFF");
  return 1;
}

int keithley2410::voltage_sweep(serial_interface *si)
{
  si->set_address(m_address);

  double applied_voltage = this->read_voltage(si);
  //  std::cout<<"Target voltage: "<<m_voltage[m_operating_channel]<<"[V]"<<std::endl;
  std::cout<<"Applied voltage: "<<applied_voltage<<"[V]"<<std::endl;
  std::cout<<"Voltage step size: "<<(m_voltage[m_operating_channel]-applied_voltage)/m_sweep_steps<<"[V]"<<std::endl;
  int diffV = fabs(applied_voltage-m_voltage[m_operating_channel]);
  if(diffV<m_sweep_steps){
    std::cout<<"#steps: "<<diffV<<" (reduced because the min voltage step=1[V])"<<std::endl;
  }else{
    std::cout<<"#steps: "<<m_sweep_steps<<std::endl;
  }

  int nsteps = std::min(m_sweep_steps,diffV);
  for(int istep=0; istep<nsteps; istep++){
    double tmp_voltage = round(applied_voltage+(m_voltage[m_operating_channel]-applied_voltage)/nsteps*(istep+1));
    if(istep==nsteps-1) tmp_voltage = m_voltage[m_operating_channel];
    si->write(":SOURCE:VOLTAGE:LEVEL "+std::to_string(tmp_voltage));
    std::cout<<"#"<<istep<<": Applied voltage: "<<this->read_voltage(si)<<"[V]"<<std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(m_sweep_sleep_in_ms));
  }

  return 0;
};

int keithley2410::config_voltage(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":SOURCE:VOLTAGE:LEVEL "+std::to_string(m_voltage[m_operating_channel]));
  return 0;
}

int keithley2410::config_compliance(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance[m_operating_channel]));
  return 0;
}

bool keithley2410::is_on(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":OUTPUT?");
  si->make_talker();
  si->read(buffer);
  si->make_listener();
  if(buffer.substr(0,1)=="0") return false;
  return true;
}

bool keithley2410::is_off(serial_interface *si)
{
  return (not this->is_on(si));
}

double keithley2410::read_voltage(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":READ?");
  si->make_talker();
  si->read(buffer);
  double voltage = std::stod(buffer.substr(0,13));
  si->make_listener();

  return voltage;
}

double keithley2410::read_current(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":READ?");
  si->make_talker();
  si->read(buffer);
  double current = std::stod(buffer.substr(14,13));
  si->make_listener();

  return current;
}

void keithley2410::read_voltage_and_current(serial_interface *si, double &voltage, double &current)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":READ?");
  si->make_talker();
  si->read(buffer);
  voltage = std::stod(buffer.substr(0,13));
  current = std::stod(buffer.substr(14,13));
  si->make_listener();

  return;
}
