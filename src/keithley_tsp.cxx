#include<iostream>
#include<sys/select.h>
#include<unistd.h>
#include<string.h>
#include"keithley_tsp.h"

keithley_tsp::keithley_tsp(){};
keithley_tsp::~keithley_tsp(){};

int keithley_tsp::create_tcp_socket(){
  std::cout<<"Create socket for TCP...";
  tcpsock = socket(AF_INET, SOCK_STREAM, 0);
  if(tcpsock < 0){
    perror("TCP socket");
    std::cout<<"errno = "<<errno<<std::endl;
    exit(1);
  }
  memset(&tcpAddr, 0, sizeof(tcpAddr));
  tcpAddr.sin_family      = AF_INET;
  tcpAddr.sin_port        = htons(tcpPort);
  tcpAddr.sin_addr.s_addr = inet_addr(sitcpIpAddr.c_str());
  std::cout<<"  Done"<<std::endl;

  std::cout<<"  ->Trying to connect to "<<sitcpIpAddr<<" ..."<<std::endl;
  if(connect(tcpsock, (struct sockaddr *)&tcpAddr, sizeof(tcpAddr)) < 0){
    if(errno != EINPROGRESS) perror("TCP connection");
    FD_ZERO(&rmask);
    FD_SET(tcpsock, &rmask);
    wmask = rmask;
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;

    int rc = select(tcpsock+1, &rmask, NULL, NULL, &timeout);
    if(rc<0) perror("connect-select error");
    if(rc==0){
      puts("\n     =====time out !=====\n ");
      exit(1);
    }
    else{
      puts("\n     ===connection error===\n ");
      exit(1);
    }
  }
  FD_ZERO(&readfds);
  FD_SET(tcpsock, &readfds);
  FD_SET(0, &readfds);

  puts("  ->Connect success!");

  return true;
}

int keithley_tsp::close_tcp_socket(){
  std::cout<<"Close TCP Socket...";
  close(tcpsock);
  std::cout<<"  Done"<<std::endl;
  return true;
}

int keithley_tsp::initialize()
{
  this->create_tcp_socket();
  this->write("localnode.prompts=1\r\n");

//   m_address = -1;
//  m_initialized = false;

//   m_serial = new SerialCom(m_device_name);
//   if(not m_serial->is_initialized()) return -1;
//   std::string command = "++mode 1\r\n";
// #ifdef DEBUG
//   std::cout<<this->get_device_name()<<": "<<command<<std::endl;
// #endif
//   m_serial->write(command);

  m_initialized = true;
  return 0;
};

int keithley_tsp::finalize()
{
  this->close_tcp_socket();
  //  delete m_serial;
  return 0;
};

int keithley_tsp::set_address(int address)
{
//   m_address = address;
//   //  std::string command = "++addr "+std::to_string(address)+R"(\r\n)";
//   std::string command = "++addr "+std::to_string(address)+"\r\n";
// #ifdef DEBUG
//   std::cout<<this->get_device_name()<<": "<<command<<std::endl;
// #endif
//   m_serial->write(command);

  return 0;
}

int keithley_tsp::write(std::string command)
{
  send(get_tcp_socket(), command.c_str(), command.length(), 0);
// #ifdef DEBUG
//   std::cout<<this->get_device_name()<<": "<<command<<std::endl;
// #endif
//   m_serial->write(command);
  return 0;
}

std::string keithley_tsp::read(std::string &buffer)
{
  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec = 500000;
  fd_set fdset;
  FD_ZERO(&fdset);
  FD_SET(get_tcp_socket(), &fdset);
  fcntl(get_tcp_socket(), F_SETFL, O_NONBLOCK);
  int ntime_out(0);
  int msg_len(0);
  int read_len(0);
  std::string data_stream="";

  int select_flag = select(get_tcp_socket()+1, &fdset, NULL, NULL, &timeout);
  if(select_flag == 0){
    //    std::cout << ">>>>>>>>>> RdRegister() : select() time out <<<<<<<<<<" << std::endl;
    ntime_out++;
    if(ntime_out>3){
      std::cout << "read() : Many timeouts... There should be some problems." << std::endl;
      exit(1);
    }
  }
  else if(select_flag < 0) return 0;

  usleep(500);
  //  getchar();
  int status = ioctl(get_tcp_socket(), FIONREAD, &msg_len);
  if(status<0) perror("### ERROR: Sock::readNum:ioctl fatal error");
  //  std::cout << "message length =" << msg_len << std::endl;
  char recv_buf[msg_len];

  int rlen = recv(get_tcp_socket(), &recv_buf, msg_len, 0);

  buffer = recv_buf;
  return "";
}

int keithley_tsp::make_listener()
{
//   std::string command = "++auto 0\r\n";
// #ifdef DEBUG
//   std::cout<<this->get_device_name()<<": "<<command<<std::endl;
// #endif
//   m_serial->write(command);
  return 0;
}

int keithley_tsp::make_talker()
{
//   std::string command = "++auto 1\r\n";
// #ifdef DEBUG
//   std::cout<<this->get_device_name()<<": "<<command<<std::endl;
// #endif
//   m_serial->write(command);

  return 0;
}
