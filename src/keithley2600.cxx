#include<sstream>
#include<iterator>
#include<iostream>
#include<vector>
#include<chrono>
#include<thread>
#include<cmath>

#include"serial_interface.h"
#include"keithley_tsp.h"
#include"keithley2600.h"

int keithley2600::initialize(serial_interface *si)
{
  return 0;
}

int keithley2600::finalize(serial_interface *si)
{
  return 0;
}

int keithley2600::reset(serial_interface *si)
{
  si->set_address(m_address);
  si->write("*RST\r\n");
  return 0;
}

int keithley2600::configure(serial_interface *si)
{
  si->set_address(m_address);
  si->make_listener();

//   //Making Voltage source mode with fixed Voltage
//   si->write(":SOURCE:FUNCTION VOLTAGE\r\n");
//   si->write(":SOURCE:VOLTAGE:MODE FIXED\r\n");
//   si->write(":SOURCE:VOLTAGE:RANGE MAX\r\n");

  //Measurement settings
  si->write("smu"+m_str_channel+".measure.autorangei = smu"+m_str_channel+".AUTORANGE_ON\r\n");
  si->write("smu"+m_str_channel+".measure.autorangev = smu"+m_str_channel+".AUTORANGE_ON\r\n");

  //Configuring Voltage and the current compliance.
  si->write("smu"+m_str_channel+".source.limiti = "+std::to_string(m_compliance[m_operating_channel])+"\r\n");
//   si->write(":SOURCE:VOLTAGE:LEVEL "+std::to_string(m_voltage[m_operating_channel])+"\r\n");

  return 0;
}

int keithley2600::power_on(serial_interface *si)
{
  si->set_address(m_address);
  si->write("smu"+m_str_channel+".source.output = 1\r\n");

  return 1;
}

int keithley2600::power_off(serial_interface *si)
{
  si->set_address(m_address);
  si->write("smu"+m_str_channel+".source.output = 0\r\n");
  return 1;
}

int keithley2600::voltage_sweep(serial_interface *si)
{
  si->set_address(m_address);

  std::cout<<"before"<<std::endl;
  double applied_voltage = this->read_voltage(si);
  std::cout<<"after"<<std::endl;
  //  std::cout<<"Target voltage: "<<m_voltage[m_operating_channel]<<"[V]"<<std::endl;
  std::cout<<"Applied voltage: "<<applied_voltage<<"[V]"<<std::endl;
  std::cout<<"Voltage step size: "<<(m_voltage[m_operating_channel]-applied_voltage)/m_sweep_steps<<"[V]"<<std::endl;
  std::cout<<"#steps: "<<m_sweep_steps<<std::endl;

  for(int istep=0; istep<m_sweep_steps; istep++){
    double tmp_voltage = round(applied_voltage+(m_voltage[m_operating_channel]-applied_voltage)/m_sweep_steps*(istep+1));
    if(istep==m_sweep_steps-1) tmp_voltage = m_voltage[m_operating_channel];
    si->write("smu"+m_str_channel+".source.levelv = "+std::to_string(tmp_voltage)+"\r\n");
    std::cout<<"#"<<istep<<": Applied voltage: "<<this->read_voltage(si)<<"[V]"<<std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(m_sweep_sleep_in_ms));
  }

  return 0;
};

int keithley2600::config_voltage(serial_interface *si)
{
  si->set_address(m_address);
  si->write("smu"+m_str_channel+".source.levelv = "+std::to_string(m_voltage[m_operating_channel])+"\r\n");
  return 0;
}

int keithley2600::config_compliance(serial_interface *si)
{
  si->set_address(m_address);
  si->write("smu"+m_str_channel+".source.limiti = "+std::to_string(m_compliance[m_operating_channel])+"\r\n");
  return 0;
}

void keithley2600::set_operating_channel(int operating_channel)
{
  m_str_channel = (operating_channel==0)?"a":(operating_channel==1?"b":"");
  m_operating_channel = operating_channel;
}

bool keithley2600::is_on(serial_interface *si)
{
//   std::string buffer;
//   si->set_address(m_address);
//   si->write(":OUTPUT?\r\n");
//   si->make_talker();
//   si->read(buffer);
//   si->make_listener();
//   if(buffer.substr(0,1)=="0") return false;
  return true;
}

bool keithley2600::is_off(serial_interface *si)
{
  return (not this->is_on(si));
}

double keithley2600::read_voltage(serial_interface *si)
{
  double voltage = 0;
  std::string buffer;
//   si->set_address(m_address);
  si->read(buffer);//making the TCP buffer empty. Some charactors like TSP prompts are there...
  si->write("print(smu"+m_str_channel+".measure.v())\r\n");
  si->read(buffer);

  std::stringstream ss(buffer);
  std::istream_iterator<std::string> begin(ss);
  std::istream_iterator<std::string> end;
  std::vector<std::string> vstrings(begin,end);
  //  std::copy(vstrings.begin(), vstrings.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
  for(uint icnt=0; icnt<vstrings.size()-1; icnt++){
    //    std::cout<<vstrings.at(icnt).find("?")<<std::endl;
    //    std::cout<<"Word="<<icnt<<": "<<vstrings.at(icnt)<<std::endl;
    if(vstrings.at(icnt).find("?")==std::string::npos){
      //      std::cout<<"number!!"<<std::endl;
      //      std::cout<<std::stod(vstrings.at(icnt))<<std::endl;
      voltage = std::stod(vstrings.at(icnt));
      break;
    }else{
      //      std::cout<<"hogehoge"<<std::endl;
      continue;
    }
  }

  return voltage;
}

double keithley2600::read_current(serial_interface *si)
{
  double current = 0;
  std::string buffer;
  //   si->set_address(m_address);
  si->read(buffer);//making the TCP buffer empty. Some charactors like TSP prompts are there...
  si->write("print(smu"+m_str_channel+".measure.i())\r\n");
  si->read(buffer);

  std::stringstream ss(buffer);
  std::istream_iterator<std::string> begin(ss);
  std::istream_iterator<std::string> end;
  std::vector<std::string> vstrings(begin,end);
  //  std::copy(vstrings.begin(), vstrings.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
  for(uint icnt=0; icnt<vstrings.size()-1; icnt++){
    //    std::cout<<"Word="<<icnt<<": "<<vstrings.at(icnt)<<std::endl;
    if(vstrings.at(icnt).find("?")==std::string::npos){
      current = std::stod(vstrings.at(icnt));
      break;
    }else{
      continue;
    }
  }

  return current;
}

void keithley2600::read_voltage_and_current(serial_interface *si, double &voltage, double &current)
{
  voltage = this->read_voltage(si);
  current = this->read_current(si);

  return;
}
