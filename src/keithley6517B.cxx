#include<iostream>
#include<chrono>
#include<thread>
#include<cmath>

#include"serial_interface.h"
#include"keithley6517B.h"
#include"SerialCom.h"

int keithley6517B::initialize()
{
  return 0;
}

int keithley6517B::finalize()
{
  return 0;
}

int keithley6517B::reset(serial_interface *si)
{
  si->set_address(m_address);
  si->write("*RST\r\n");
  return 0;
}

int keithley6517B::configure(serial_interface *si)
{
  si->set_address(m_address);
  si->make_listener();

  //Making Voltage source mode
  si->write(":SOURCE:VOLT:RANGE 1000\r\n");
  si->write(":SOURCE:VOLT:MCONNECT ON\r\n");// ammeter connected

  //Measurement settings
  si->write(":SENSE:FUNCTION \"CURR\"\r\n");
  si->write(":SENSE:CURRENT:RANGE:AUTO ON\r\n");

  //Does this command exist...?
  si->write(":SYST:ZCHECK OFF\r\n");// zero-check off

  // //Configuring Voltage and the current compliance.
  // si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance[m_operating_channel])+"\r\n");
  // si->write(":SOURCE:VOLTAGE "+std::to_string(m_voltage[m_operating_channel])+"\r\n");

  return 0;
}

int keithley6517B::power_on(serial_interface *si)
{
  si->set_address(m_address);
  si->write("OUTPUT ON\r\n");

  return 1;
}

int keithley6517B::power_off(serial_interface *si)
{
  si->set_address(m_address);
  si->write("OUTPUT OFF\r\n");
  return 1;
}

int keithley6517B::voltage_sweep(serial_interface *si)
{
  si->set_address(m_address);

  double applied_voltage = this->read_voltage(si);
  //  std::cout<<"Target voltage: "<<m_voltage[m_operating_channel]<<"[V]"<<std::endl;
  std::cout<<"Applied voltage: "<<applied_voltage<<"[V]"<<std::endl;
  std::cout<<"Voltage step size: "<<(m_voltage[m_operating_channel]-applied_voltage)/m_sweep_steps<<"[V]"<<std::endl;
  std::cout<<"#steps: "<<m_sweep_steps<<std::endl;

  for(int istep=0; istep<m_sweep_steps; istep++){
    double tmp_voltage = round(applied_voltage+(m_voltage[m_operating_channel]-applied_voltage)/m_sweep_steps*(istep+1));
    if(istep==m_sweep_steps-1) tmp_voltage = m_voltage[m_operating_channel];
    si->write(":SOURCE:VOLTAGE "+std::to_string(tmp_voltage)+"\r\n");
    std::cout<<"#"<<istep<<": Applied voltage: "<<this->read_voltage(si)<<"[V]"<<std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(m_sweep_sleep_in_ms));
  }

  return 0;
};

int keithley6517B::config_voltage(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":SOURCE:VOLTAGE "+std::to_string(m_voltage[m_operating_channel])+"\r\n");
  return 0;
}

int keithley6517B::config_compliance(serial_interface *si)
{
  si->set_address(m_address);
  si->write(":SENSE:CURRENT:PROTECTION "+std::to_string(m_compliance[m_operating_channel])+"\r\n");
  return 0;
}

bool keithley6517B::is_on(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":OUTPUT?\r\n");
  si->make_talker();
  si->read(buffer);
  si->make_listener();
  if(buffer.substr(0,1)=="0") return false;
  return true;
}

bool keithley6517B::is_off(serial_interface *si)
{
  return (not this->is_on(si));
}

double keithley6517B::read_voltage(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":MEASURE:CURRENT?\r\n");
  si->make_talker();
  si->read(buffer);
  double voltage = std::stod(buffer.substr(0,13));
  si->make_listener();

  return voltage;
}

double keithley6517B::read_current(serial_interface *si)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":MEASURE:CURRENT?\r\n");
  si->make_talker();
  si->read(buffer);
  double current = std::stod(buffer.substr(14,13));
  si->make_listener();

  return current;
}

void keithley6517B::read_voltage_and_current(serial_interface *si, double &voltage, double &current)
{
  std::string buffer;
  si->set_address(m_address);
  si->write(":MEASURE:CURRENT?\r\n");
  si->make_talker();
  si->read(buffer);
  voltage = std::stod(buffer.substr(0,13));
  current = std::stod(buffer.substr(14,13));
  si->make_listener();

  return;
}
