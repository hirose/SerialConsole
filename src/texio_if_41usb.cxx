#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<termios.h>
#include<iostream>
#include<unistd.h>
#include<string.h>
#include"texio_if_41usb.h"

texio_if_41usb::texio_if_41usb(){};
texio_if_41usb::~texio_if_41usb()
{
  this->write("LC1");
};

#define BAUDRATE B9600

int texio_if_41usb::initialize()
{
  fd = open(m_device_name.c_str(), O_RDWR | O_NOCTTY );
  if(fd<0){
    perror(m_device_name.c_str());
    return 1;
  }

  tcgetattr(fd, &oldtio); // Preserve current serial port settings.
  bzero(&newtio, sizeof(newtio)); //Clear the new seral port struct.

  newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
  newtio.c_iflag = IGNPAR | ICRNL;
  newtio.c_oflag = 0;
  newtio.c_lflag = ICANON;
// }else if(m_itype==rs232){
//     //    newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
//     //    newtio.c_cflag = BAUDRATE | CS7 | CREAD | CRTSCTS | CLOCAL;
//     newtio.c_cflag = BAUDRATE | CS7 | CREAD | PARENB;
//     newtio.c_iflag = IGNPAR | ICRNL;
//     newtio.c_oflag = 0;
//     newtio.c_lflag = ICANON;
//   }
  // newtio.c_iflag = IGNPAR | ICRNL;
  // newtio.c_oflag = 0;
  // newtio.c_lflag = ICANON | ECHO;

  newtio.c_cc[VINTR]    = 0; // Ctrl-c
  newtio.c_cc[VQUIT]    = 0; // Ctrl-\
  newtio.c_cc[VERASE]   = 0; // del
  newtio.c_cc[VKILL]    = 0; // @
  newtio.c_cc[VEOF]     = 4; // Ctrl-d
  newtio.c_cc[VTIME]    = 0; // No in-character timer
  newtio.c_cc[VMIN]     = 1; // Blocking until receiving a character
  newtio.c_cc[VSWTC]    = 0; // '\0'
  newtio.c_cc[VSTART]   = 0; // Ctrl-q
  newtio.c_cc[VSTOP]    = 0; // Ctrl-s
  newtio.c_cc[VSUSP]    = 0; // Ctrl-z
  newtio.c_cc[VEOL]     = 0; // '\0'
  newtio.c_cc[VREPRINT] = 0; // Ctrl-r
  newtio.c_cc[VDISCARD] = 0; // Ctrl-u
  newtio.c_cc[VWERASE]  = 0; // Ctrl-w
  newtio.c_cc[VLNEXT]   = 0; // Ctrl-v
  newtio.c_cc[VEOL2]    = 0; // '\0'

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &newtio);

  usleep(2000);

  m_initialized = true;
  return 0;
};

int texio_if_41usb::finalize()
{
  tcsetattr(fd, TCSANOW, &oldtio);
  return 0;
};

int texio_if_41usb::write(std::string command)
{
  buf=const_cast<char*>(command.c_str());
  ::write(fd, buf, 255);
  usleep(100000);
  //  std::cout<<"command: "<<command<<std::endl;

  return 0;
}

std::string texio_if_41usb::read(std::string &buffer)
{
  char buf[255];
  ::read(fd, buf, 255);
  buffer = buf;

  return "";
}

int texio_if_41usb::make_listener()
{
  return 0;
}

int texio_if_41usb::make_talker()
{
  return 0;
}
