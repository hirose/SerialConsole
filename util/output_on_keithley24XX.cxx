#include<iostream>
#include<fstream>
#include<unistd.h>

#include"utility.h"
#include"picojson.h"
#include"keithley2410.h"
#include"prologix_gpibusb.h"
#include"keithley_24XX_rs232.h"

int main(int argc, char* argv[])
{
  //Checking if a config file is given
  if(argc!=2){
    std::cout<<"Usage: "<<argv[0]<<" path/to/config.json"<<std::endl;
    return 1;
  }

  //Retrieve contents of the config file
  std::string configjson = argv[1];
  std::ifstream ifs(configjson.c_str(), std::ios::in);
  if(ifs.fail()){
    std::cout<<"Error: Failed to read "<<configjson<<std::endl;
    return 1;
  }
  const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
  ifs.close();

  //Parsing the config file
  picojson::value v;
  const std::string err = picojson::parse(v, json);
  if (err.empty() == false) {
    std::cout<<"Something wrong in "<<configjson<<std::endl;
    std::cout<<err<<std::endl;
    return 1;
  }
  picojson::object& obj = v.get<picojson::object>();

  //Checking if necessary values are set in the config
  if(obj["devicefile"].is<picojson::null>()){
    std::cout<<"\"devicefile\": is not set in "<<configjson<<std::endl;;
    return 1;
  }
  if(obj["address"].is<picojson::null>()){
    std::cout<<"\"address\": is not set in "<<configjson<<std::endl;;
    return 1;
  }
  if(obj["interfacetype"].is<picojson::null>()){
    std::cout<<"\"interfacetype\": is not set in "<<configjson<<std::endl;;
    return 1;
  }

  //Showing contents of the config file
  std::cout<<"Control based on settings below ============="<<std::endl;
  std::cout<<"devicefile: "<<obj["devicefile"].get<std::string>()<<std::endl;
  std::cout<<"address: "<<obj["address"].get<double>()<<std::endl;
  std::cout<<"interfacetype: "<<obj["interfacetype"].get<std::string>()<<std::endl;
  if(not obj["devicetype"].is<picojson::null>()){
    std::cout<<"devicetype: "<<obj["devicetype"].get<std::string>()<<std::endl;
  }
  picojson::array& array = obj["channels"].get<picojson::array>();
  for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
    picojson::object& tmp = it->get<picojson::object>();
    std::cout<<"Output "<<tmp["channel"].get<std::string>()<<": "<<
      ((not tmp["voltage"].is<picojson::null>())?std::to_string(tmp["voltage"].get<double>()).substr(0,5)+"[V]":"null")<<", "<<
      ((not tmp["current"].is<picojson::null>())?std::to_string(tmp["current"].get<double>()).substr(0,5)+"[A]":"null")<<std::endl;
  }

  std::string device_name = obj["devicefile"].get<std::string>();

  //Making serial interface for devices
  serial_interface* serial_interface;
  if(obj["interfacetype"].get<std::string>()=="RS232"){
    serial_interface = new keithley_24XX_rs232();
  }else if(obj["interfacetype"].get<std::string>()=="GPIB"){
    serial_interface = new prologix_gpibusb();
  }else{
    std::cout<<"[ERROR] Interface type should be \"RS232\" or \"GPIB\"."<<std::endl;
    return -1;
  }

  serial_interface->set_device_name(device_name);
  serial_interface->initialize();
  if(not serial_interface->is_initialized()){
    std::cout<<"[ERROR] failed to create an instance of the serial interface for "<<device_name<<"."<<std::endl;
    return -1;
  }

  //Making power supply instance
  power_supply* power_supply = new keithley2410(obj["address"].get<double>());
  if(power_supply->is_on(serial_interface)){
    std::cout<<"Output is already enabled."<<std::endl;
    return -1;
  }
  power_supply->configure(serial_interface);

  for(picojson::array::iterator it = array.begin(); it != array.end(); it++){
    picojson::object& tmp = it->get<picojson::object>();
    int channel = -1;
    if     (tmp["channel"].get<std::string>()=="A") channel = 0;
    else if(tmp["channel"].get<std::string>()=="B") channel = 1;
    else if(tmp["channel"].get<std::string>()=="C") channel = 2;
    else if(tmp["channel"].get<std::string>()=="D") channel = 3;
    else std::cout<<"Warning: Channel should be in range A-D ("<<tmp["channel"].get<std::string>()<<" was given)."<<std::endl;
    if(channel!=-1){
      power_supply->set_enable(true, channel);
      if(tmp["voltage"].is<picojson::null>()){
        power_supply->set_voltage(0., channel);
        power_supply->set_enable(false, channel);
      }
      else power_supply->set_voltage(tmp["voltage"].get<double>(),channel);
      if(tmp["current"].is<picojson::null>()){
        power_supply->set_compliance(0., channel);
        power_supply->set_enable(false, channel);
      }
      else power_supply->set_compliance(tmp["current"].get<double>(),channel);
    }
  }

  power_supply->power_on(serial_interface);

  std::cout<<"Output enabled."<<std::endl;

  //Reading voltage and current
  double voltage(0.), current(0.);
  power_supply->read_voltage_and_current(serial_interface, voltage, current);

  //Output information
  std::string out_format = get_out_format(voltage, current);
  std::cout<<out_format<<std::endl;

  delete serial_interface;
  delete power_supply;

  return 0;
}
