#include<iostream>
#include<fstream>
#include<chrono>
#include<ctime>
#include<thread>
#include<fcntl.h>
#include<termios.h>
#include<unistd.h>

#include"keithley2600.h"
//#include"prologix_gpibusb.h"
#include"keithley_tsp.h"
#include"utility.h"

int nNo=99;

void ReadIV(power_supply *ps, serial_interface *si)
{
  if(not ps->is_on(si)){
    printf("Voltage and current reading can be performed only when the output is enabled. Doing nothing...\n\n");
    return;
  }

  clearStdin();
  int trial=0;
  printf("==============================================\n");
  printf("Hit any key to return to the prompt.\n");
  printf("==============================================\n");
  int tmp_chan = ps->get_operating_channel();

  while(1){
    //Reading voltage and current
    double voltage(0.), current(0.);
    ps->set_operating_channel(0);
    ps->read_voltage_and_current(si, voltage, current);
    //Preparing output file
    std::ofstream ofile_keithley_2410_1(ps->get_ofile_name()+"_A", std::ofstream::app);
    std::string out_format1 = get_out_format(voltage, current);
    ofile_keithley_2410_1<<out_format1<<std::endl;
    std::cout<<"Channel A: "<<out_format1<<std::endl;

    //Preparing output file
    ps->set_operating_channel(1);
    ps->read_voltage_and_current(si, voltage, current);
    std::ofstream ofile_keithley_2410_2(ps->get_ofile_name()+"_B", std::ofstream::app);
    std::string out_format2 = get_out_format(voltage, current);
    ofile_keithley_2410_2<<out_format2<<std::endl;
    std::cout<<"Channel B: "<<out_format2<<std::endl;

    if(kbhit()) break;
    trial++;
    sleep(1);
  }
  ps->set_operating_channel(tmp_chan);
  clearStdin();

  return;
}

void SelectFunction(){
  printf("====================================================\n");
  printf("Please specify what you want to do... \n");
  printf(" 0: Initializing Keithley2410 \n");
  printf(" 1: Reset Keithley2410 \n");
  printf(" 2: Configure HV value\n");
  printf(" 3: Sweep HV value to the target\n");
  printf(" 4: Output ON\n");
  printf(" 5: Output OFF\n");
  printf(" 6: Read voltage/current\n");
  printf(" 7: Change operating channel\n");
  printf("100: Change #sweep steps\n");
  printf("101: Change sweep interval\n");
  //  printf(" 7: Read voltage/current for all working devices\n");
  //  printf("20: IV measurement\n");
  printf("99: exit \n");

  while(1){
    nNo=999;
    printf("No = ");
    scanf("%d",&nNo);
    if(nNo==999) clearStdin();
    printf("\n");
    if(nNo >= 0) break;
  }

  return;
}

int CallFunction(power_supply *ps, serial_interface *si){
  int id = 999;
  int addr = 0;
  float HV = 10000;
  int para = -1;
  switch(nNo){
  case 0:
    printf("=== Configuare Keithley2410.\n");
    ps->configure(si);
    break;
  case 1:
    printf("=== Reset Keithley2410.\n");
    ps->reset(si);
    break;
  case 2:
    printf("=== Set HV value.\n");
    printf("Hit desired voltage [V]: ");
    scanf("%f",&HV);
    if(HV==10000){ //Fail-safe for someone who hit non-numeric value
      printf("Hit a number...");
      clearStdin();
      break;
    }
    ps->set_voltage(HV);
    ps->config_voltage(si);
    break;
  case 3:
    if(not ps->is_on(si)){
      printf("Voltage sweep is only available with output ON. Doing nothing...\n\n");
      break;
    }
    printf("=== Voltage sweep to the target value.\n");
    printf("Hit desired voltage [V]: ");
    scanf("%f",&HV);
    if(HV==10000){ //Fail-safe for someone who hit non-numeric value
      printf("Hit a number...");
      clearStdin();
      break;
    }
    std::cout<<"Configuring Voltage to "<<HV<<" [V]"<<std::endl;
    ps->set_voltage(HV);
    ps->voltage_sweep(si);
    break;
  case 4:
    printf("=== Set output ON.\n");
    ps->power_on(si); //Output ON with initial voltage
    break;
  case 5:
    printf("=== Set output OFF.\n");
    ps->power_off(si); //Output ON with initial voltage
    break;
  case 6:
    printf("=== Read voltage and current.\n");
    ReadIV(ps,si);
    break;
  case 7:
    printf("=== Change an operation channel.\n");
    printf("Hit \"0\" for [Channel A] or \"1\" for [Channel B]: ");
    scanf("%d",&para);
    if(para!=0 and para!=1){ //Fail-safe for someone who hit non-numeric value
      printf("Invalid number...");
      clearStdin();
      break;
    }
    ps->set_operating_channel(para);
    break;
  case 100:
    printf("=== Change #sweep steps.\n");
    printf("Hit desired steps: ");
    scanf("%d",&para);
    if(para<0){ //Fail-safe for someone who hit non-numeric value
      printf("Invalid number...");
      clearStdin();
      break;
    }
    std::cout<<"Configuring #sweep steps to "<<para<<std::endl;
    ps->set_sweep_steps(para);
    break;
  case 101:
    printf("=== Change #sweep sleep time.\n");
    printf("Hit desired sleep time [ms]: ");
    scanf("%d",&para);
    if(para<0){ //Fail-safe for someone who hit non-numeric value
      printf("Invalid number...");
      clearStdin();
      break;
    }
    std::cout<<"Configuring sweep sleep time to "<<para<<std::endl;
    ps->set_sweep_sleep_in_ms(para);
    break;
  // case 7:
  //   printf("=== Monitoring voltage and current for all working keithleys.\n");
  //   ReadIVAll();
  //   break;
  // case 20:
  //   printf("=== IV measurement.\n");
  //   printf("Hit address:");
  //   scanf("%d",&addr);
  //   if(addr<1 || 15<=addr){
  //     printf("Please specify address in range (2-15).");
  //   }else{
  //     char tmpaddr[5];
  //     sprintf(tmpaddr, "%d", addr);
  //     IVMeasure(tmpaddr);
  //   }
  //   break;
  case 99:
    return 1;
  default:
    printf("not defined. doing nothing...\n");
    break;
  }
  printf("\n");

  return 0;
}

int main()
{
  std::cout<<"Started PowerSupply control application."<<std::endl;

  //Making serial interface for devices
  serial_interface* serial_interface = new keithley_tsp();
  std::string ipaddr = "192.168.60.253";
  //  serial_interface->SetIPPort(.c_str(),23);
  serial_interface->set_ip_address(ipaddr);
  serial_interface->set_tcp_port(23);
  serial_interface->set_udp_port(0);

//   getchar();
//   serial_interface->initialize();
// //   getchar();
// //   serial_interface->write("beeper.enable = beeper.ON\r\n");
// //   getchar();
// //   serial_interface->write("beeper.beep(0.5,2500)\r\n");
//   getchar();
//   serial_interface->write("smua.source.output = 1\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 1\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 2\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 3\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 4\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 5\r\n");


//   getchar();
//   serial_interface->write("smua.source.levelv = 4\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 3\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 2\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 1\r\n");
//   getchar();
//   serial_interface->write("smua.source.levelv = 0\r\n");
//   getchar();
//   serial_interface->write("smua.source.output = 0\r\n");

  serial_interface->initialize();
  if(not serial_interface->is_initialized()){
    std::cout<<"[ERROR] failed to create an instance of the serial interface for "<<serial_interface->get_device_name()<<"."<<std::endl;
    return -1;
  }
  
//   //Making devices to control
//   int keithley2600_1_address = 24;
  double keithley2600_1_compliance = 10.E-6;
  double keithley2600_1_output_voltage = 80.;
  power_supply* keithley2600_1 = new keithley2600();
  keithley2600_1->set_voltage(5.0); //initial voltage
  keithley2600_1->set_compliance(keithley2600_1_compliance);
  keithley2600_1->set_sweep_steps(20);
  keithley2600_1->set_sweep_sleep_in_ms(500);
//  keithley2600_1->configure(serial_interface); //Configualing device
//   // keithley2600_1->power_on(serial_interface); //Output ON with initial voltage

//   // //Voltage sweep to the config voltage
//   // keithley2600_1->set_voltage(keithley2600_1_output_voltage);
//   // keithley2600_1->voltage_sweep(serial_interface);
//   // std::this_thread::sleep_for(std::chrono::milliseconds(500));

  //Preparing output file
  std::ofstream ofile_keithley_2600_1(keithley2600_1->get_ofile_name()+"_A", std::ofstream::app);
  std::ofstream ofile_keithley_2600_2(keithley2600_1->get_ofile_name()+"_B", std::ofstream::app);

  //Reading voltage and current
  double voltage(0.), current(0.);

//   //Output information
//   std::string out_format = get_out_format(voltage, current, keithley2600_1_compliance);
//   ofile_keithley_2600_1<<out_format<<std::endl;
//   std::cout<<out_format<<std::endl;

//   //Finalizing operation
//   keithley2600_1->set_voltage(0.);
//   keithley2600_1->voltage_sweep(serial_interface);
//   keithley2600_1->power_off(serial_interface);

  while(1){
    SelectFunction();
    int ret = CallFunction(keithley2600_1, serial_interface);
    if(ret!=0) break;
  }

  serial_interface->finalize();
  delete serial_interface;
//   delete keithley2600_1;

  std::cout<<"Closing application..."<<std::endl;

  return 0;
}
